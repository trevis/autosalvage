﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MyClasses.MetaViewWrappers;

namespace AutoSalvage
{
    //Attaches events from core
    [WireUpBaseEvents]

    // FriendlyName is the name that will show up in the plugins list of the decal agent (the one in windows, not in-game)
    // View is the path to the xml file that contains info on how to draw our in-game plugin. The xml contains the name and icon our plugin shows in-game.
    // The view here is SamplePlugin.mainView.xml because our projects default namespace is SamplePlugin, and the file name is mainView.xml.
    // The other key here is that mainView.xml must be included as an embeded resource. If its not, your plugin will not show up in-game.
    [FriendlyName("AutoSalvage")]
    public class PluginCore : PluginBase {

        /// <summary>
        /// This is called when the plugin is started up. This happens only once.
        /// </summary>
        protected override void Startup()
		{
			try
			{
				// This initializes our static Globals class with references to the key objects your plugin will use, Host and Core.
				// The OOP way would be to pass Host and Core to your objects, but this is easier.
				Globals.Init("AutoSalvage", Host, Core);
			}
			catch (Exception ex) { Util.LogError(ex); }
		}

		/// <summary>
		/// This is called when the plugin is shut down. This happens only once.
		/// </summary>
		protected override void Shutdown()
		{
			try
			{
                CoreManager.Current.CommandLineText -= new EventHandler<ChatParserInterceptEventArgs>(Current_CommandLineText);
                CoreManager.Current.RenderFrame -= new EventHandler<EventArgs>(Current_RenderFrame);
            }
            catch (Exception ex) { Util.LogError(ex); }
		}

		[BaseEvent("LoginComplete", "CharacterFilter")]
		private void CharacterFilter_LoginComplete(object sender, EventArgs e)
		{
			try
			{
                CoreManager.Current.CommandLineText += new EventHandler<ChatParserInterceptEventArgs>(Current_CommandLineText);
                CoreManager.Current.RenderFrame += new EventHandler<EventArgs>(Current_RenderFrame);
            }
            catch (Exception ex) { Util.LogError(ex); }
		}

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                AutoSalvage.Think();
            }
            catch (Exception ex) { Util.LogError(ex); }
        }

        private void Current_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            try {
                if (e.Text.StartsWith("/autosalvage")) {
                    bool force = e.Text.StartsWith("/autosalvage force");
                    e.Eat = true;

                    AutoSalvage.Start(force);

                    return;
                }
            }
            catch (Exception ex) { Util.LogError(ex); }
        }
    }
}
