# It's dead, Jim

This project is no longer being updated and has been rolled into [UtilityBelt](https://gitlab.com/trevis/utilitybelt).

# About

This plugin will attempt to salvage all items in your inventory that match loot rule salvage in your currently loaded loot profile.  This is helpful for cleaning up after vtank misses salvaging items.  It needs to ID all items in your inventory so it may take a minute to run.  It avoids salvaging equipped or tinkered items, anything else that matches a salvage loot rule is fair game, **you have been warned**.

# Notes

**Use at your own risk, I'm not responsible if you salvage your super dope untinkered armor that was in your inventory.**

**Make sure `/config SalvageMultiple` is set to true**

# Installation

[AutoSalvageInstaller-0.0.0.2.exe](/uploads/c8b5223aa0739be88da07c399f89b2ad/AutoSalvageInstaller-0.0.0.2.exe)

# Usage  

`/autosalvage` - adds all salvage items to the salvage window

`/autosalvage force` - adds all salvage items to the salvage window AND hits the salvage button.