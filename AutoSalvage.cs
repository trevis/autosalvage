﻿using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace AutoSalvage {
    class AutoSalvage {
        private static List<int> inventoryItems;
        private static List<int> salvageItemIds;
        private static bool isRunning = false;
        private static bool shouldSalvage = false;

        private static DateTime lastThought = DateTime.MinValue;
        private static bool readyToSalvage = false;

        public static void Start(bool force=false) {
            isRunning = true;
            shouldSalvage = force;

            Reset();

            LoadInventory();
        }

        public static void Reset() {
            inventoryItems = new List<int>();
            salvageItemIds = new List<int>();
            readyToSalvage = false;
        }

        public static void LoadInventory() {
            var inventory = Globals.Core.WorldFilter.GetInventory();
            int requestedIdsCount = 0;

            foreach (var item in inventory) {
                inventoryItems.Add(item.Id);

                if (NeedsID(item.Id)) {
                    requestedIdsCount++;
                    Globals.Core.Actions.RequestId(item.Id);
                }
            }

            if (requestedIdsCount > 0) {
                Util.WriteToChat(String.Format("Requesting id data for {0}/{1} inventory items. This will take approximately {2} seconds.", requestedIdsCount, inventoryItems.Count, requestedIdsCount));
            }
        }

        private static bool NeedsID(int id) {
            return uTank2.PluginCore.PC.FLootPluginQueryNeedsID(id);
        }

        private static void OpenSalvageWindow() {
            foreach (var id in inventoryItems) {
                var item = Globals.Core.WorldFilter[id];

                if (item != null && item.Name == "Ust") {
                    Globals.Core.Actions.UseItem(id, 0);
                }
            }
        }

        private static void AddSalvageToWindow() {
            int salvageableItemCount = 0;

            foreach (var id in inventoryItems) {
                try {
                    var result = uTank2.PluginCore.PC.FLootPluginClassifyImmediate(id);

                    var item = Globals.Core.WorldFilter[id];

                    if (item == null) continue;

                    // If the item is equipped or wielded, don't process it.
                    if (item.Values(LongValueKey.EquippedSlots, 0) > 0 || item.Values(LongValueKey.Slot, -1) == -1)
                        continue;

                    // If the item is tinkered don't process it
                    if (item.Values(LongValueKey.NumberTimesTinkered, 0) > 1) continue;

                    // If the item is imbued don't process it
                    if (item.Values(LongValueKey.Imbued, 0) > 1) continue;

                    // dont put in bags of salvage
                    if (item.Name.StartsWith("Salvage")) continue;

                    if (result.SimpleAction == uTank2.eLootAction.Salvage) {
                        Globals.Core.Actions.SalvagePanelAdd(id);
                        salvageableItemCount++;
                        Util.WriteToChat(String.Format("Salvage: {0}", item.Name));
                    }
                }
                catch (Exception ex) { Util.LogError(ex); }
            }

            if (salvageableItemCount > 0) {
                Util.WriteToChat(String.Format("Found {0} items to salvage.", salvageableItemCount));
            }
            else {
                Util.WriteToChat("No salvageable items found.");
            }

            readyToSalvage = true;
        }


        [DllImport("Decal.dll")]
        static extern int DispatchOnChatCommand(ref IntPtr str, [MarshalAs(UnmanagedType.U4)] int target);

        public static bool Decal_DispatchOnChatCommand(string cmd) {
            IntPtr bstr = Marshal.StringToBSTR(cmd);

            try {
                bool eaten = (DispatchOnChatCommand(ref bstr, 1) & 0x1) > 0;

                return eaten;
            }
            finally {
                Marshal.FreeBSTR(bstr);
            }
        }

        public static void Think() {
            if (DateTime.UtcNow - lastThought > TimeSpan.FromMilliseconds(1000)) {
                lastThought = DateTime.UtcNow;

                if (isRunning) {
                    bool hasAllItemData = true;

                    foreach (var id in inventoryItems) {
                        if (NeedsID(id)) {
                            hasAllItemData = false;
                            break;
                        }
                    }

                    if (readyToSalvage) {
                        readyToSalvage = false;
                        isRunning = false;

                        if (shouldSalvage) {
                            Globals.Core.Actions.SalvagePanelSalvage();
                        }

                        Decal_DispatchOnChatCommand("AutoSalvage complete.");
                        Globals.Host.Actions.AddChatText("AutoSalvage complete.", 5);

                        return;
                    }

                    if (isRunning && hasAllItemData && !readyToSalvage) {
                        OpenSalvageWindow();
                        AddSalvageToWindow();
                        lastThought = DateTime.UtcNow;
                    }
                }
            }
        }
    }
}
